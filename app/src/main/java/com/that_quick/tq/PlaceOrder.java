package com.that_quick.tq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PlaceOrder extends AppCompatActivity {

    GetDataService service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);


        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);


        Button confirmorder = findViewById(R.id.confirmorder);
        confirmorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                placeOrder(

                        VarData.userphone,
                        VarData.tqservice,
                        VarData.tqsubservice,
                        "Uttara, Dhaka",
                        "Dec 8, 2PM",
                        "Be on time, dont be late"

                );


            }
        });

    }


    void placeOrder(String phone, String tqservice, String subservice,
                    String location, String preftime, String adnote) {


        service.placeOrder(phone, tqservice, subservice, location, preftime, adnote).
                enqueue(new Callback<PlaceOrderModel>() {

            @Override
            public void onResponse(Call<PlaceOrderModel> call, Response<PlaceOrderModel> response) {


                Log.d("ebb", response.errorBody()+"");


                try {
                    if (response.body().getIsSuccess() == 1) {

                        Intent intent = new Intent(getApplicationContext(), Profile.class);
                        startActivity(intent);

                        Toast.makeText(getApplicationContext(), "Order placed successfully!",
                                Toast.LENGTH_LONG).show();



                    }

                } catch (NullPointerException e){

                    Toast.makeText(getApplicationContext(), "Order placing failed!" ,
                            Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<PlaceOrderModel> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Order placing failed!", Toast.LENGTH_SHORT).show();

                Log.d("res-p", t.getMessage()+" == "+t.toString());

            }
        });



    }

}
