package com.that_quick.tq;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PlaceOrderModel {

    private int isSuccess;


    @SerializedName("cphone")
    @Expose
    private String cphone;
    @SerializedName("services")
    @Expose
    private String services;
    @SerializedName("subservices")
    @Expose
    private String subservices;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("preferedtime")
    @Expose
    private String preferedtime;
    @SerializedName("additional_information")
    @Expose
    private String additionalInformation;


    public String getCphone() {
        return cphone;
    }

    public void setCphone(String cphone) {
        this.cphone = cphone;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPreferedtime() {
        return preferedtime;
    }

    public void setPreferedtime(String preferedtime) {
        this.preferedtime = preferedtime;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getSubservices() {
        return subservices;
    }

    public void setSubservices(String subservices) {
        this.subservices = subservices;
    }


    public int getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(int isSuccess) {
        this.isSuccess = isSuccess;
    }

}
