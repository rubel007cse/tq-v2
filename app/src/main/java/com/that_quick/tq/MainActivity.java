package com.that_quick.tq;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int FrValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView loginsignup = findViewById(R.id.loginsignup);
        final TextView loginsignupText = findViewById(R.id.loginsignupText);


        LogIn logIn = new LogIn();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, logIn).commit();
        loginsignupText.setText("No acctount?");
        loginsignup.setText("Signup");


        loginsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getFrValue() == 1){
                    LogIn logIn = new LogIn();
                    getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, logIn).commit();
                    loginsignupText.setText("No acctount?");
                    loginsignup.setText("Signup");

                    setFrValue(0);

                } else {
                    SignUP signUp = new SignUP();
                    getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, signUp).commit();
                    loginsignupText.setText("Has acctount?");
                    loginsignup.setText("Login");

                    setFrValue(1);
                }




            }
        });


    }


    public int getFrValue() {
        return FrValue;
    }

    public void setFrValue(int frValue) {
        FrValue = frValue;
    }
}
