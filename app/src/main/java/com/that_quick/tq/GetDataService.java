package com.that_quick.tq;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetDataService {

    @POST("api/createuser.php/")
    @FormUrlEncoded
    Call<RegistrationModel> createUser(
            @Field("fname") String fname,
            @Field("lname") String lname,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("password") String pass


    );


    @GET("api/signin.php/")
    Call<RegistrationModel> signInUser(
            @Query("PhoneNumber") String phone,
            @Query("Password") String pass);




    @POST("api/placeorder.php/")
    @FormUrlEncoded
    Call<PlaceOrderModel> placeOrder(
            @Field("cphone") String phone,
            @Field("services") String tqservice,
            @Field("subservices") String subservice,
            @Field("location") String location,
            @Field("preferedtime") String preftime,
            @Field("additional_information") String adnote



    );




}
