package com.that_quick.tq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TQSubServices extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tqsub_services);


        Button continueorder = findViewById(R.id.continueorder);
        continueorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                VarData.tqsubservice = "Kitchen Cleaning";
                startActivity(new Intent(getApplicationContext(), PlaceOrder.class));



            }
        });



    }
}
