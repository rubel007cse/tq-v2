package com.that_quick.tq;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogIn extends Fragment {


    GetDataService service;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.login,
                container, false);

        Button logInButtonId = view.findViewById(R.id.logInButtonId);
        final EditText phone = view.findViewById(R.id.phonenumber );
        final EditText password = view.findViewById(R.id.password);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        logInButtonId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phonedata = phone.getText().toString();
                String passworddata = password.getText().toString();

                // sednign data for sign in
                Log.d("loginwith", "Login with: "+phonedata+", "+passworddata);
                signinData(phonedata, passworddata);

            }
        });


        return view;

    }

    public void signinData(String PhoneNumber, String Password) {

        service.signInUser(PhoneNumber, Password).enqueue(new Callback<RegistrationModel>() {

            @Override
            public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {

                try {
                    if (response.body().getIsSuccess() == 1) {

                        Intent intent = new Intent(getActivity(), Profile.class);
                        startActivity(intent);

                        Toast.makeText(getContext(), "Login Successful!",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (NullPointerException e){

                    Toast.makeText(getContext(), "Not a valid phone number or password!" ,
                            Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<RegistrationModel> call, Throwable t) {

                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}